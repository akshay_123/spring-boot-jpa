package com.akshay.springboot.jpa.tutorial.repository;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.akshay.springboot.jpa.tutorial.entity.Course;
import com.akshay.springboot.jpa.tutorial.entity.CourseMaterial;

@SpringBootTest
class CourseDetailTest {
	
	@Autowired
	private CourseDetailRepository courseDetailRepository;

	@BeforeEach
	void setUp() throws Exception {
	}

//	@Test
	void testSaveCorseDetails() {
		Course course = Course.builder()
				.name("AL")
				.credit("7")
				.build();
		CourseMaterial courseMaterial = CourseMaterial.builder()
				.url("AL.com")
				.course(course)
				.build();
		this.courseDetailRepository.save(courseMaterial);
	}
	
	@Test
	void testGetCourseMaetrialList() {
		List<CourseMaterial> cm= this.courseDetailRepository.findAll();
		System.out.println(cm);
		cm.forEach(i->System.out.println(i.getCourse()));
		//System.out.println(cm.);
	}

}
