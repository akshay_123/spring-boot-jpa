package com.akshay.springboot.jpa.tutorial.repository;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.akshay.springboot.jpa.tutorial.entity.Student;
import com.akshay.springboot.jpa.tutorial.entity.StudentGaurdian;

@SpringBootTest
class StudentRepositoryTest {

	@Autowired
	private StudentRepository studentRepository;
	
//	@Test
	@Disabled
	public void saveStudent() {
		StudentGaurdian gaurdian= StudentGaurdian.builder()
				.name("Pankaj")
				.email("pankaj@gmail.com")
				.mobile("9130271731")
				.build();
		
		Student student = Student.builder()
				.emailId("akshay@gmail.com")
				.firstName("Akshay")
				.lastName("Chaugule")
				.gaurdian(gaurdian)
				.build();
		
		this.studentRepository.save(student);
	}
	
//	@Test
	@Disabled
	public void getSrudentByGaurdianName() {
		List<Student> studentList=this.studentRepository.findByGaurdianName("Pankaj");
		System.out.println(studentList);
	}
	
//	@Test
//	@Disabled
	public void printGetStudentByEmailAddress(){
		Student s= this.studentRepository.getStudentByEmailAddress("akshay@gmail.com");
		System.out.println(s);
		
	}
	
//	@Test
//	@Disabled
	public void printGetStudentByGaurdianEmial(){
		Student s= this.studentRepository.getStudentByGaurdianEmail("pankaj@gmail.com");
		System.out.println(s);
		
	}
	
//	@Test
//	@Disabled
	public void printGetStudentNameByEmailAddress(){
		String studentName= this.studentRepository.getStudentNameByEmailAddress("akshay@gmail.com");
		System.out.println(studentName);
		
	}
	
//	@Test
//	@Disabled
	public void printGetStudentByEmailAddressNative(){
		Student s= this.studentRepository.getStudentByEmailAddressNative("akshay@gmail.com");
		System.out.println(s);
		
	}
	
//	@Test
//	@Disabled
	public void printGetStudentByEmailAddressNativeNamedParam(){
		Student s= this.studentRepository.getStudentByEmailAddressNativeNamedParam("akshay@gmail.com");
		System.out.println(s);
		
	}
	
//	@Test
//	@Disabled
	public void printGetStudentListByCollectionParamenter(){
		List<String> nameList=new ArrayList<String>();
		nameList.add("Akshay");
		nameList.add("Pankaj");
		List<Student> s= this.studentRepository.getStudentListByCollectionParamenter(nameList);
		System.out.println(s);
	}
	
	@Test
	public void printupdateStudentNameByEmailAddressCount(){
		int affectedRows= this.studentRepository.updateStudentNameByEmailAddress("Ram","akshay@gmail.com");
		System.out.println(affectedRows);
		
	}
}
