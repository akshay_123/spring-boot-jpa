package com.akshay.springboot.jpa.tutorial.repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.akshay.springboot.jpa.tutorial.entity.Product;

@SpringBootTest
class ProductRepositoryTest {
	@Autowired
	private ProductRepository productRepository;

	@BeforeEach
	void setUp() throws Exception {
	}

//	@Test
//	void testSaveProduct() {
//		List<Product> productList= IntStream.range(1, 200)
//				.mapToObj(i->Product.builder().name("Product"+i).quantity(i).build())
//				.collect(Collectors.toList());
//		
//		this.productRepository.saveAll(productList);
//		
//	}
	
	@Test
	void testgetAllProduct() {
		List<Product> productList= this.productRepository.findAll();
		System.out.println(productList.size());
		
		
	}

}
