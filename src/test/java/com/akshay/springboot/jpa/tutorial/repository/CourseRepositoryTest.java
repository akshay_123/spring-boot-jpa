package com.akshay.springboot.jpa.tutorial.repository;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.akshay.springboot.jpa.tutorial.entity.Course;
import com.akshay.springboot.jpa.tutorial.entity.Teacher;

@SpringBootTest
class CourseRepositoryTest {

    @Autowired	
	CourseRepository courseRepository;
    
	@BeforeEach
	void setUp() throws Exception {
	}

//	@Test
	void testFetchListOfCourses() {
		List<Course> courseList= this.courseRepository.findAll();
		System.out.println(courseList);
	}
	
//	@Test
	void testSaveCourse() {
		Teacher teacher= 
				Teacher.builder()
				.name("Rohit")
				.build();
		
		Course mutual= Course.builder()
				.name("Angular")
				.credit("6")
				.teacher(teacher)
				.build();
		Course share= Course.builder()
				.name("React")
				.credit("6")
				.teacher(teacher)
				.build();
		this.courseRepository.saveAll(List.of(mutual,share));
	}

	@Test
	void getCourseBySort() {
		String sortBy="credit";
		List<Course> cousesByCredit=this.courseRepository.findAll(Sort.by(Direction.ASC,sortBy));
		System.out.println(cousesByCredit);
	}
}
