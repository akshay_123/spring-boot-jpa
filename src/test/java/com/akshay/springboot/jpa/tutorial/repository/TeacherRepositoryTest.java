package com.akshay.springboot.jpa.tutorial.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.akshay.springboot.jpa.tutorial.entity.Course;
import com.akshay.springboot.jpa.tutorial.entity.CourseMaterial;
import com.akshay.springboot.jpa.tutorial.entity.Teacher;

@SpringBootTest
class TeacherRepositoryTest {

	@Autowired
	private TeacherRepository teacherRepository;
	@BeforeEach
	void setUp() throws Exception {
	}

//	@Test
	public void saveTeacher() {
		
		Course dba= Course.builder()
				.name("java")
				.credit("6")
				.build();
		
		Course java= Course.builder()
				.name(".net")
				.credit("6")
				.build();
		
		Teacher teacher= 
				Teacher.builder()
				.name("Ravi")
				.courses(List.of(dba,java))
				.build();
		this.teacherRepository.save(teacher);
	}
	@Test
	private void test() {
		System.out.println("No");
		
	}

}
