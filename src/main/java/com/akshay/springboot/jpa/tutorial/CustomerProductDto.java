package com.akshay.springboot.jpa.tutorial;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_DEFAULT)
public class CustomerProductDto {
	private String customerName;
	private Integer quantity;
	private String productName;
	
	
	//If we want to bind only 4 properties and dont; want num
	public CustomerProductDto(String customerName, Integer quantity, String productName) {
		super();
		this.customerName = customerName;
		this.quantity = quantity;
		this.productName = productName;
	}

	private int num;

}
