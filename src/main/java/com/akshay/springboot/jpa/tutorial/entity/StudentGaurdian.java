package com.akshay.springboot.jpa.tutorial.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AttributeOverrides({
	@AttributeOverride(name = "name",column = @Column(name ="gaurdian_name")),
	@AttributeOverride(name = "email",column = @Column(name ="gaurdian_email")),
	@AttributeOverride(name = "mobile",column = @Column(name ="gaurdian_mobile")),
})
public class StudentGaurdian {
	private String name;
	private String email;
	private String mobile;

}
