package com.akshay.springboot.jpa.tutorial.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akshay.springboot.jpa.tutorial.CustomerProductDto;
import com.akshay.springboot.jpa.tutorial.entity.Customer;
import com.akshay.springboot.jpa.tutorial.repository.CustomerRepository;

@RestController
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@PostMapping("/")
	public Customer saveCustomer(@RequestBody Customer customer){
		return this.customerRepository.save(customer);
		
	}
	
	@GetMapping("/")
	public List<Customer> getAllCustomer(){
		return this.customerRepository.findAll();	
	}
	
	@GetMapping("/get_join_info")
	public List<CustomerProductDto> getDataUsingJoin(){
		return this.customerRepository.getDataUsingJoin();
		
	}

}
