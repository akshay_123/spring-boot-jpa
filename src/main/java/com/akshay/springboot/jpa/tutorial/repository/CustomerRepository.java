package com.akshay.springboot.jpa.tutorial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.akshay.springboot.jpa.tutorial.CustomerProductDto;
import com.akshay.springboot.jpa.tutorial.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	@Query("SELECT new com.akshay.springboot.jpa.tutorial.CustomerProductDto(c.name as customerName,p.quantity,p.name as productName) "
			+ "from Customer c join c.products p")
	List<CustomerProductDto> getDataUsingJoin();
	

}
