package com.akshay.springboot.jpa.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akshay.springboot.jpa.tutorial.entity.Product;
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
