package com.akshay.springboot.jpa.tutorial.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.akshay.springboot.jpa.tutorial.service.ProductService;

@RestController
public class SortAndPaginationController {
	@Autowired
	private ProductService ProductService;
	
	@GetMapping("/{sort}")
	public Object getAllProductAfterSorting(@PathVariable("sort") String sort){
		return this.ProductService.getAllProductAfterSorting(sort);
		
	}
	
	@GetMapping("/pagination/{offset}/{pageSize}")
	public Object getAllProductsWitPagination(@PathVariable("offset") int offset, @PathVariable("pageSize") int pageSize){
		return this.ProductService.getAllProductsWitPagination(offset,pageSize);
		
	}
	
	@GetMapping("/pagination/{offset}/{pageSize}/{sort}")
	public Object getAllProductsWitPaginationAndSorting(
			@PathVariable("offset") int offset,
			@PathVariable("pageSize") int pageSize,@PathVariable("sort") String field){
		return this.ProductService.getAllProductsWitPaginationAndSorting(offset,pageSize,field);
		
	}

}
