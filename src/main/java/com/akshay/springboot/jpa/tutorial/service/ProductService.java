package com.akshay.springboot.jpa.tutorial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.akshay.springboot.jpa.tutorial.entity.Product;
import com.akshay.springboot.jpa.tutorial.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	private ProductRepository productRepository;
	//sorting
	public List<Product> getAllProductAfterSorting(String sort){
	//	return this.productRepository.findAll(Sort.by(sort).descending());
		return this.productRepository.findAll(Sort.by(sort).descending().and(Sort.by("id")));
	}
	
	//pagiantion
	public List<Product> getAllProductsWitPagination(int offset, int pageSize){
		Pageable pageWithOffsetAndPageSize = PageRequest.of(offset,pageSize);
		Page<Product> products= this.productRepository.findAll(pageWithOffsetAndPageSize);
		return products.getContent();
		
		}
	
	//Paginationwithsorting
	public Page<Product> getAllProductsWitPaginationAndSorting(int offset, int pageSize,String field){
		Pageable pageWithOffsetAndPageSize = PageRequest.of(offset,pageSize,Sort.by(field));
		Page<Product> products= this.productRepository.findAll(pageWithOffsetAndPageSize);
		return products;
		
		}

}
