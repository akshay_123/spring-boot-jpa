package com.akshay.springboot.jpa.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akshay.springboot.jpa.tutorial.entity.CourseMaterial;

@Repository
public interface CourseDetailRepository extends JpaRepository<CourseMaterial, Long> {

}
