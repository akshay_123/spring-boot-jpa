package com.akshay.springboot.jpa.tutorial.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.akshay.springboot.jpa.tutorial.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	
	//Method query
	List<Student> findByGaurdianName (String gaurdianName);
	
	//JPQL 
	@Query("select s from Student s where s.emailId=?1")
	Student getStudentByEmailAddress(String emailAddress);
	
	@Query("select s from Student s where s.gaurdian.email=?1")
	Student getStudentByGaurdianEmail(String gaurdianEmial);
	
	@Query("select s.firstName from Student s where s.emailId=?1")
	String getStudentNameByEmailAddress(String email);
	
	@Query("select s from Student s where s.firstName IN :nameList")
	List<Student> getStudentListByCollectionParamenter(@Param("nameList") List<String> nameList);
	
	@Modifying
	@Transactional
	@Query("update Student s set s.firstName= :name where s.emailId= :emailAddress")
	int updateStudentNameByEmailAddress(@Param("name") String name,
			@Param("emailAddress") String emailAddress);
	
	//Native SQL query 
	@Query(
			value = "select * from student s where s.email_address=?1",
			nativeQuery = true
			)
	Student getStudentByEmailAddressNative(String emailAddress);
	
	//Native Named Param SQL query 
	@Query(
			value = "select * from student s where s.email_address= :emailAddress",
			nativeQuery = true
			)
	Student getStudentByEmailAddressNativeNamedParam(@Param("emailAddress") String emailAddress);

}
